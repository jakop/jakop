# Initial Design Ideas


## What is JaKOp

`JaKOp` stems from the need to have an equivalent of the [Operator Framework](https://coreos.com/operators/) but for Java. CoreOS' operator framework is only available for the Go programming language --also Ansible and Helm, but no other programming language. We, as part of the Java Community, want to be able to write Kubernetes Operators in Java.

An operator is no more than an specialized application that calls Kubernetes APIs. As such, it can be built on any programming language. And given the breadth of the Java ecosystem and its popularity, it makes sense to provide the basic infrastructure and the common best practices that are required to build an operator. `JaKOp` should be just a dependency that Java programs may use to simplify the task of building Kubernetes operators.

`JaKOp` is an effort that was started as part of a discussion at the amazing unConference [jCrete](jCrete.org) of 2019.


## Mailing list, chat channel

* [Public development mailing list](https://groups.google.com/forum/#!forum/jakop).

* [Slack channel](https://jakopdev.slack.com). Request access if you want to join.

* (Future) [website](https://jakop.dev).
